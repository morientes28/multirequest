var zmq = require('zeromq')
  , sock = zmq.socket('pub');

const config = require('./config.json');
const log4js = require('./log4js');

var logger = log4js.getLogger();
 
sock.bindSync('tcp://127.0.0.1:3000');

logger.debug('Publisher bound to port 3000');

let workers = config.workers_request;
let n = Object.keys(workers).length;

logger.debug('Count of workers:', n);
logger.debug('Gap between request is:', config.gap_request,'ms');
 
let counter = 0;
let stop = false;
setInterval(function(){
  counter++;
  if (!stop){
  	logger.info('Sending a start trigger for sub', counter);
  	sock.send([counter, JSON.stringify(workers[counter])]);
  }
  if (counter == n)
  	stop = true;
  	//process.exit(1);
}, config.gap_request);
