#!/bin/bash
#

clear
rm -f logs.log
pm2 delete all
pm2 flush
clear

printf "\nStarting workers...\n\n"
sleep 1
pm2 start sub.js --name sub1 -- 1
sleep 2
pm2 start sub.js --name sub2 -- 2

printf "\nStarting publisher...\n\n"
sleep 2
pm2 start pub.js

printf "\nShowing progress...\n\n"
sleep 6

cat logs.log