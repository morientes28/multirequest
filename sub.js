var zmq = require('zeromq')
  , sock = zmq.socket('sub');
const moment = require('moment');
const request = require('request');
const log4js = require('./log4js');
const config = require('./config.json');

var logger = log4js.getLogger();

if (process.argv.length <= 2){
	logger.error("Subscriber has not set a ID (first argument in command)");
	process.exit(1);
} 
sock.connect('tcp://127.0.0.1:3000');
sock.subscribe(process.argv[2]);

logger.debug("Subscriber connected to port 3000");
logger.info("Subscriber :", process.argv[2])

function postToServer(data){
	request({
		method: 'POST',
	    uri: config.request_url,
	    json: true,
	    body: JSON.parse(data)
	},function (error, response, body) {
	    	if (error) {
		      return console.error('upload failed:', error);
		    }
			let date = moment().format('hh:mm:ss.SSS');
	        logger.info(date, "- Response: ", body);
	        //process.exit(1);
	    }
	);
}
 
sock.on('message', function(topic, message) {
  let date = moment().format('hh:mm:ss.SSS');
  let r = message.toString('ascii').replace(/\\/g,"");
  //let data = { json: { r } };
  logger.info(date, "- Request: ", r);
  postToServer(r);
});

