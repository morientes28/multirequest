#!/bin/bash

# $WORKSPACE need to be included !

# synchronization production branches with master branch

BRANCH_PREFIX='production'

pushd $WORKSPACE > /dev/null

    PRODUCTION_BRANCHES=$(git branch -r | grep $BRANCH_PREFIX | cut -c 10-)
    git fetch --all

    for branch in $PRODUCTION_BRANCHES;
        do
            printf "\nSynchronization branch $branch with master\n"
            
            git checkout -b $branch origin/$branch
            git pull origin $branch
            git rebase master
            git push origin $branch --force-with-lease
            if  [ $? -eq 0 ] ; then
                printf "\nSynchronized branch $branch has been pushed to origin\n"
                git checkout master
                git branch -D $branch
            else
                printf "\nError in synchronization branch $branch\n"
                exit 1;
            fi
        done

popd > /dev/null